package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PrivateTalkJpa extends JpaRepository<PublicTalkJpa, Integer>, JpaSpecificationExecutor<PublicTalkJpa> {
}
