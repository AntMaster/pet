package dao;


import domain.bean.PetSearch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface PetSearchJpa extends JpaRepository<PetSearch,Integer> ,JpaSpecificationExecutor<PetSearch> {


}
