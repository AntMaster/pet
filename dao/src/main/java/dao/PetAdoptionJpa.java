package dao;


import domain.bean.PetAdoption;
import domain.bean.PetSearch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface PetAdoptionJpa extends JpaRepository<PetAdoption, String>,JpaSpecificationExecutor<PetSearch> {

}
