package domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import domain.bean.PetSearch;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberDto {

    private int id;
    private String openId;
    private int areaId;
    private String name;
    private String nickName;
    private String headImgUrl;
    private boolean gender;
    private String mobile;
    private String email;
    private String address;
    private String remark;
    private int donationNum;
    private BigDecimal donationTotal;
    private String qrCode;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp modifyDate;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createDate;
    private String appId;
    private int approveState;
    private int approveType;

    private List<PetSearch> searchList;

    private Map<Integer, PetSearch> searchMap;

}
