package domain.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import domain.bean.Member;
import lombok.Data;

import java.sql.Timestamp;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PetSearchDto {

    @JsonFormat
    private int id;
    private String publisherId;
    private Integer publishType;
    private Integer classifyId;
    private Integer varietyId;
    private String petName;
    private Integer petSex;
    private String petDescription;
    private String petImage;
    private Timestamp lostTime;
    private String lostLocation;
    private Double latitude;
    private Double longitude;
    private String ownerName;
    private String ownerContact;
    private Integer reward;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp rewardBegintime;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp rewardEndtime;
    private Integer findState;
    private Integer viewCount;
    private Integer shareCount;
    private Integer publishState;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Integer publishNo;

    private Member member;


}
