package domain.bean;


import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "UserTalk", schema = "dbo", catalog = "pawlove")
public class PrivateTalk {
    private int id;
    private Integer talkId;
    private String userIdFrom;
    private String userIdAccept;
    private String content;
    private Timestamp talkTime;
    private Integer publishId;
    private String publisherId;
    private Integer readState;
    private Integer showState;
    private Timestamp lastModify;
    private String adoptionId;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TalkID", nullable = true)
    public Integer getTalkId() {
        return talkId;
    }

    public void setTalkId(Integer talkId) {
        this.talkId = talkId;
    }

    @Basic
    @Column(name = "UserIDFrom", nullable = true, length = 50)
    public String getUserIdFrom() {
        return userIdFrom;
    }

    public void setUserIdFrom(String userIdFrom) {
        this.userIdFrom = userIdFrom;
    }

    @Basic
    @Column(name = "UserIDAccept", nullable = true, length = 50)
    public String getUserIdAccept() {
        return userIdAccept;
    }

    public void setUserIdAccept(String userIdAccept) {
        this.userIdAccept = userIdAccept;
    }

    @Basic
    @Column(name = "Content", nullable = true, length = 500)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "TalkTime", nullable = true)
    public Timestamp getTalkTime() {
        return talkTime;
    }

    public void setTalkTime(Timestamp talkTime) {
        this.talkTime = talkTime;
    }

    @Basic
    @Column(name = "PublishID", nullable = true)
    public Integer getPublishId() {
        return publishId;
    }

    public void setPublishId(Integer publishId) {
        this.publishId = publishId;
    }

    @Basic
    @Column(name = "PublisherID", nullable = true, length = 50)
    public String getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    @Basic
    @Column(name = "ReadState", nullable = true)
    public Integer getReadState() {
        return readState;
    }

    public void setReadState(Integer readState) {
        this.readState = readState;
    }

    @Basic
    @Column(name = "ShowState", nullable = true)
    public Integer getShowState() {
        return showState;
    }

    public void setShowState(Integer showState) {
        this.showState = showState;
    }

    @Basic
    @Column(name = "LastModify", nullable = true)
    public Timestamp getLastModify() {
        return lastModify;
    }

    public void setLastModify(Timestamp lastModify) {
        this.lastModify = lastModify;
    }

    @Basic
    @Column(name = "adoption_id", nullable = true, length = 50)
    public String getAdoptionId() {
        return adoptionId;
    }

    public void setAdoptionId(String adoptionId) {
        this.adoptionId = adoptionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrivateTalk privateTalk = (PrivateTalk) o;
        return id == privateTalk.id &&
                Objects.equals(talkId, privateTalk.talkId) &&
                Objects.equals(userIdFrom, privateTalk.userIdFrom) &&
                Objects.equals(userIdAccept, privateTalk.userIdAccept) &&
                Objects.equals(content, privateTalk.content) &&
                Objects.equals(talkTime, privateTalk.talkTime) &&
                Objects.equals(publishId, privateTalk.publishId) &&
                Objects.equals(publisherId, privateTalk.publisherId) &&
                Objects.equals(readState, privateTalk.readState) &&
                Objects.equals(showState, privateTalk.showState) &&
                Objects.equals(lastModify, privateTalk.lastModify) &&
                Objects.equals(adoptionId, privateTalk.adoptionId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, talkId, userIdFrom, userIdAccept, content, talkTime, publishId, publisherId, readState, showState, lastModify, adoptionId);
    }
}
