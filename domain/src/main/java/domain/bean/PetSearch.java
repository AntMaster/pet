package domain.bean;


import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "petpublish", schema = "dbo", catalog = "pawlove")
@EntityListeners(AuditingEntityListener.class)
public class PetSearch {
    private int id;
    private String publisherId;
    private Integer publishType;
    private Integer classifyId;
    private Integer varietyId;
    private String petName;
    private Integer petSex;
    private String petDescription;
    private String petImage;
    private Timestamp lostTime;
    private String lostLocation;
    private Double latitude;
    private Double longitude;
    private String ownerName;
    private String ownerContact;
    private Integer reward;
    private Timestamp rewardBegintime;
    private Timestamp rewardEndtime;
    private Integer findState;
    private Integer publishState;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Integer publishNo;
    private String petColor;
    private Integer areaId;
    private String areaName;
    private Timestamp amountBegin;
    private Timestamp amountEnd;
    private Integer amount;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "publisher_id", nullable = true, length = 50)
    public String getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    @Basic
    @Column(name = "publish_type", nullable = true)
    public Integer getPublishType() {
        return publishType;
    }

    public void setPublishType(Integer publishType) {
        this.publishType = publishType;
    }

    @Basic
    @Column(name = "classify_id", nullable = true)
    public Integer getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(Integer classifyId) {
        this.classifyId = classifyId;
    }

    @Basic
    @Column(name = "variety_id", nullable = true)
    public Integer getVarietyId() {
        return varietyId;
    }

    public void setVarietyId(Integer varietyId) {
        this.varietyId = varietyId;
    }

    @Basic
    @Column(name = "pet_name", nullable = true, length = 50)
    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    @Basic
    @Column(name = "pet_sex", nullable = true)
    public Integer getPetSex() {
        return petSex;
    }

    public void setPetSex(Integer petSex) {
        this.petSex = petSex;
    }

    @Basic
    @Column(name = "pet_description", nullable = true, length = 500)
    public String getPetDescription() {
        return petDescription;
    }

    public void setPetDescription(String petDescription) {
        this.petDescription = petDescription;
    }

    @Basic
    @Column(name = "pet_image", nullable = true, length = 500)
    public String getPetImage() {
        return petImage;
    }

    public void setPetImage(String petImage) {
        this.petImage = petImage;
    }

    @Basic
    @Column(name = "lost_time", nullable = true)
    public Timestamp getLostTime() {
        return lostTime;
    }

    public void setLostTime(Timestamp lostTime) {
        this.lostTime = lostTime;
    }

    @Basic
    @Column(name = "lost_location", nullable = true, length = 50)
    public String getLostLocation() {
        return lostLocation;
    }

    public void setLostLocation(String lostLocation) {
        this.lostLocation = lostLocation;
    }

    @Basic
    @Column(name = "latitude", nullable = true, precision = 0)
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "longitude", nullable = true, precision = 0)
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "owner_name", nullable = true, length = 50)
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Basic
    @Column(name = "owner_contact", nullable = true, length = 50)
    public String getOwnerContact() {
        return ownerContact;
    }

    public void setOwnerContact(String ownerContact) {
        this.ownerContact = ownerContact;
    }

    @Basic
    @Column(name = "reward", nullable = true, precision = 0)
    public Integer getReward() {
        return reward;
    }

    public void setReward(Integer reward) {
        this.reward = reward;
    }

    @Basic
    @Column(name = "reward_begintime", nullable = true)
    public Timestamp getRewardBegintime() {
        return rewardBegintime;
    }

    public void setRewardBegintime(Timestamp rewardBegintime) {
        this.rewardBegintime = rewardBegintime;
    }

    @Basic
    @Column(name = "reward_endtime", nullable = true)
    public Timestamp getRewardEndtime() {
        return rewardEndtime;
    }

    public void setRewardEndtime(Timestamp rewardEndtime) {
        this.rewardEndtime = rewardEndtime;
    }

    @Basic
    @Column(name = "find_state", nullable = true)
    public Integer getFindState() {
        return findState;
    }

    public void setFindState(Integer findState) {
        this.findState = findState;
    }


    @Basic
    @Column(name = "publish_state", nullable = true)
    public Integer getPublishState() {
        return publishState;
    }

    public void setPublishState(Integer publishState) {
        this.publishState = publishState;
    }

    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }


    @CreatedDate
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "update_time", nullable = true)
    @LastModifiedDate
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "publish_no", nullable = true)
    public Integer getPublishNo() {
        return publishNo;
    }

    public void setPublishNo(Integer publishNo) {
        this.publishNo = publishNo;
    }

    @Basic
    @Column(name = "pet_color", nullable = true, length = 50)
    public String getPetColor() {
        return petColor;
    }

    public void setPetColor(String petColor) {
        this.petColor = petColor;
    }

    @Basic
    @Column(name = "area_id", nullable = true)
    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    @Basic
    @Column(name = "area_name", nullable = true, length = 50)
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Basic
    @Column(name = "amount_begin", nullable = true)
    public Timestamp getAmountBegin() {
        return amountBegin;
    }

    public void setAmountBegin(Timestamp amountBegin) {
        this.amountBegin = amountBegin;
    }

    @Basic
    @Column(name = "amount_end", nullable = true)
    public Timestamp getAmountEnd() {
        return amountEnd;
    }

    public void setAmountEnd(Timestamp amountEnd) {
        this.amountEnd = amountEnd;
    }

    @Basic
    @Column(name = "amount", nullable = true)
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PetSearch that = (PetSearch) o;
        return id == that.id &&
                Objects.equals(publisherId, that.publisherId) &&
                Objects.equals(publishType, that.publishType) &&
                Objects.equals(classifyId, that.classifyId) &&
                Objects.equals(varietyId, that.varietyId) &&
                Objects.equals(petName, that.petName) &&
                Objects.equals(petSex, that.petSex) &&
                Objects.equals(petDescription, that.petDescription) &&
                Objects.equals(petImage, that.petImage) &&
                Objects.equals(lostTime, that.lostTime) &&
                Objects.equals(lostLocation, that.lostLocation) &&
                Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(ownerName, that.ownerName) &&
                Objects.equals(ownerContact, that.ownerContact) &&
                Objects.equals(reward, that.reward) &&
                Objects.equals(rewardBegintime, that.rewardBegintime) &&
                Objects.equals(rewardEndtime, that.rewardEndtime) &&
                Objects.equals(findState, that.findState) &&
                Objects.equals(publishState, that.publishState) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(updateTime, that.updateTime) &&
                Objects.equals(publishNo, that.publishNo) &&
                Objects.equals(petColor, that.petColor) &&
                Objects.equals(areaId, that.areaId) &&
                Objects.equals(areaName, that.areaName) &&
                Objects.equals(amountBegin, that.amountBegin) &&
                Objects.equals(amountEnd, that.amountEnd) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, publisherId, publishType, classifyId, varietyId, petName, petSex, petDescription, petImage, lostTime, lostLocation, latitude, longitude, ownerName, ownerContact, reward, rewardBegintime, rewardEndtime, findState, publishState, createTime, updateTime, publishNo);
    }

    /*@Autowired
    private PetSearchRepository*/


}
