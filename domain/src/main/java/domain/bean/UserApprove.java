package domain.bean;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class UserApprove {
    private int id;
    private String userId;
    private Integer approveType;
    private String organizationName;
    private String dutyer;
    private String dutyerPhone;
    private String dutyerNo;
    private String credentials;
    private Integer approveState;
    private String description;
    private String organizationImage;
    private Timestamp createTIme;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "UserID", nullable = true, length = 50)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "ApproveType", nullable = true)
    public Integer getApproveType() {
        return approveType;
    }

    public void setApproveType(Integer approveType) {
        this.approveType = approveType;
    }

    @Basic
    @Column(name = "OrganizationName", nullable = true, length = 50)
    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    @Basic
    @Column(name = "Dutyer", nullable = true, length = 50)
    public String getDutyer() {
        return dutyer;
    }

    public void setDutyer(String dutyer) {
        this.dutyer = dutyer;
    }

    @Basic
    @Column(name = "DutyerPhone", nullable = true, length = 50)
    public String getDutyerPhone() {
        return dutyerPhone;
    }

    public void setDutyerPhone(String dutyerPhone) {
        this.dutyerPhone = dutyerPhone;
    }

    @Basic
    @Column(name = "DutyerNo", nullable = true, length = 50)
    public String getDutyerNo() {
        return dutyerNo;
    }

    public void setDutyerNo(String dutyerNo) {
        this.dutyerNo = dutyerNo;
    }

    @Basic
    @Column(name = "Credentials", nullable = true, length = 50)
    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    @Basic
    @Column(name = "ApproveState", nullable = true)
    public Integer getApproveState() {
        return approveState;
    }

    public void setApproveState(Integer approveState) {
        this.approveState = approveState;
    }

    @Basic
    @Column(name = "Description", nullable = true, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "OrganizationImage", nullable = true, length = 500)
    public String getOrganizationImage() {
        return organizationImage;
    }

    public void setOrganizationImage(String organizationImage) {
        this.organizationImage = organizationImage;
    }

    @Basic
    @Column(name = "CreateTIme", nullable = true)
    public Timestamp getCreateTIme() {
        return createTIme;
    }

    public void setCreateTIme(Timestamp createTIme) {
        this.createTIme = createTIme;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserApprove that = (UserApprove) o;
        return id == that.id &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(approveType, that.approveType) &&
                Objects.equals(organizationName, that.organizationName) &&
                Objects.equals(dutyer, that.dutyer) &&
                Objects.equals(dutyerPhone, that.dutyerPhone) &&
                Objects.equals(dutyerNo, that.dutyerNo) &&
                Objects.equals(credentials, that.credentials) &&
                Objects.equals(approveState, that.approveState) &&
                Objects.equals(description, that.description) &&
                Objects.equals(organizationImage, that.organizationImage) &&
                Objects.equals(createTIme, that.createTIme);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userId, approveType, organizationName, dutyer, dutyerPhone, dutyerNo, credentials, approveState, description, organizationImage, createTIme);
    }
}
