package domain.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;


import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "pet_adoption", schema = "dbo", catalog = "pawlove")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class PetAdoption implements Serializable {

    private static final long serialVersionUID = -2406830768590535699L;

    private String id;
    private String petNo;
    private String petNick;
    private Integer petClassify;
    private Integer petVariety;
    private Integer petSex;
    private Integer petAge;
    private Integer contraception;
    private String petImage;
    private String petDescription;
    private String organization;
    private String contacts;
    private String mobile;
    private String address;
    private String require;
    private Integer adoptState;
    private Integer showState;
    private Timestamp createTime;
    private Timestamp modifyTime;
    private String petId;
    private Integer adoptFrom;
    private String openId;
    private Integer userpetId;
    private Integer areaId;
    private String areaName;
    private Integer isPay;
    private BigDecimal amount;
    private Integer vaccineState;

    /*@Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")*/

    @Id
    @GeneratedValue(generator = "uuid.hex", strategy = IDENTITY)
    @GenericGenerator(name = "uuid.hex", strategy = "uuid.hex")
    @Column(name = "id", nullable = false, length = 50)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "pet_no", nullable = true, length = 50)
    public String getPetNo() {
        return petNo;
    }

    public void setPetNo(String petNo) {
        this.petNo = petNo;
    }

    @Basic
    @Column(name = "pet_nick", nullable = true, length = 50)
    public String getPetNick() {
        return petNick;
    }

    public void setPetNick(String petNick) {
        this.petNick = petNick;
    }

    @Basic
    @Column(name = "pet_classify", nullable = true)
    public Integer getPetClassify() {
        return petClassify;
    }

    public void setPetClassify(Integer petClassify) {
        this.petClassify = petClassify;
    }

    @Basic
    @Column(name = "pet_variety", nullable = true)
    public Integer getPetVariety() {
        return petVariety;
    }

    public void setPetVariety(Integer petVariety) {
        this.petVariety = petVariety;
    }

    @Basic
    @Column(name = "pet_sex", nullable = true)
    public Integer getPetSex() {
        return petSex;
    }

    public void setPetSex(Integer petSex) {
        this.petSex = petSex;
    }

    @Basic
    @Column(name = "pet_age", nullable = true)
    public Integer getPetAge() {
        return petAge;
    }

    public void setPetAge(Integer petAge) {
        this.petAge = petAge;
    }

    @Basic
    @Column(name = "contraception", nullable = true)
    public Integer getContraception() {
        return contraception;
    }

    public void setContraception(Integer contraception) {
        this.contraception = contraception;
    }

    @Basic
    @Column(name = "pet_image", nullable = true, length = 200)
    public String getPetImage() {
        return petImage;
    }

    public void setPetImage(String petImage) {
        this.petImage = petImage;
    }

    @Basic
    @Column(name = "pet_description", nullable = true, length = 500)
    public String getPetDescription() {
        return petDescription;
    }

    public void setPetDescription(String petDescription) {
        this.petDescription = petDescription;
    }

    @Basic
    @Column(name = "organization", nullable = true, length = 50)
    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @Basic
    @Column(name = "contacts", nullable = true, length = 50)
    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    @Basic
    @Column(name = "mobile", nullable = true, length = 50)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 100)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "require", nullable = true, length = 200)
    public String getRequire() {
        return require;
    }

    public void setRequire(String require) {
        this.require = require;
    }

    @Basic
    @Column(name = "adopt_state", nullable = true)
    public Integer getAdoptState() {
        return adoptState;
    }

    public void setAdoptState(Integer adoptState) {
        this.adoptState = adoptState;
    }

    @Basic
    @Column(name = "show_state", nullable = true)
    public Integer getShowState() {
        return showState;
    }

    public void setShowState(Integer showState) {
        this.showState = showState;
    }

    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "modify_time", nullable = true)
    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Basic
    @Column(name = "pet_id", nullable = true, length = 50)
    public String getPetId() {
        return petId;
    }

    public void setPetId(String petId) {
        this.petId = petId;
    }

    @Basic
    @Column(name = "adopt_from", nullable = true)
    public Integer getAdoptFrom() {
        return adoptFrom;
    }

    public void setAdoptFrom(Integer adoptFrom) {
        this.adoptFrom = adoptFrom;
    }

    @Basic
    @Column(name = "open_id", nullable = true, length = 50)
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Basic
    @Column(name = "userpet_id", nullable = true)
    public Integer getUserpetId() {
        return userpetId;
    }

    public void setUserpetId(Integer userpetId) {
        this.userpetId = userpetId;
    }

    @Basic
    @Column(name = "area_id", nullable = true)
    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    @Basic
    @Column(name = "area_name", nullable = true, length = 50)
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Basic
    @Column(name = "is_pay", nullable = true)
    public Integer getIsPay() {
        return isPay;
    }

    public void setIsPay(Integer isPay) {
        this.isPay = isPay;
    }

    @Basic
    @Column(name = "amount", nullable = true, precision = 5)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "vaccine_state", nullable = true)
    public Integer getVaccineState() {
        return vaccineState;
    }

    public void setVaccineState(Integer vaccineState) {
        this.vaccineState = vaccineState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PetAdoption that = (PetAdoption) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(petNo, that.petNo) &&
                Objects.equals(petNick, that.petNick) &&
                Objects.equals(petClassify, that.petClassify) &&
                Objects.equals(petVariety, that.petVariety) &&
                Objects.equals(petSex, that.petSex) &&
                Objects.equals(petAge, that.petAge) &&
                Objects.equals(contraception, that.contraception) &&
                Objects.equals(petImage, that.petImage) &&
                Objects.equals(petDescription, that.petDescription) &&
                Objects.equals(organization, that.organization) &&
                Objects.equals(contacts, that.contacts) &&
                Objects.equals(mobile, that.mobile) &&
                Objects.equals(address, that.address) &&
                Objects.equals(require, that.require) &&
                Objects.equals(adoptState, that.adoptState) &&
                Objects.equals(showState, that.showState) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(modifyTime, that.modifyTime) &&
                Objects.equals(petId, that.petId) &&
                Objects.equals(adoptFrom, that.adoptFrom) &&
                Objects.equals(openId, that.openId) &&
                Objects.equals(userpetId, that.userpetId) &&
                Objects.equals(areaId, that.areaId) &&
                Objects.equals(areaName, that.areaName) &&
                Objects.equals(isPay, that.isPay) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(vaccineState, that.vaccineState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, petNo, petNick, petClassify, petVariety, petSex, petAge, contraception, petImage, petDescription, organization, contacts, mobile, address, require, adoptState, showState, createTime, modifyTime, petId, adoptFrom, openId, userpetId, areaId, areaName, isPay, amount, vaccineState);
    }
}
