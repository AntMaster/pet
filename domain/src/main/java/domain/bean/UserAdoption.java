package domain.bean;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "user_adoption", schema = "dbo", catalog = "pawlove")
public class UserAdoption {
    private String id;
    private String adoptionId;
    private String openId;
    private String adopterName;
    private String cardNo;
    private String mobile;
    private String wechat;
    private String qq;
    private String homeAddress;
    private String workAddress;
    private String remark;
    private int adopterState;
    private Timestamp createTime;
    private Timestamp modifyTime;
    private int readState;
    private Integer animalVariety;
    private Integer income;
    private Integer live;
    private Integer areaId;
    private String areaName;
    private String cardBack;
    private String cardFront;

    @Id
    @Column(name = "id", nullable = false, length = 50)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "adoption_id", nullable = false, length = 50)
    public String getAdoptionId() {
        return adoptionId;
    }

    public void setAdoptionId(String adoptionId) {
        this.adoptionId = adoptionId;
    }

    @Basic
    @Column(name = "open_id", nullable = false, length = 50)
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Basic
    @Column(name = "adopter_name", nullable = false, length = 100)
    public String getAdopterName() {
        return adopterName;
    }

    public void setAdopterName(String adopterName) {
        this.adopterName = adopterName;
    }

    @Basic
    @Column(name = "card_no", nullable = false, length = 50)
    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    @Basic
    @Column(name = "mobile", nullable = false, length = 100)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "wechat", nullable = true, length = 50)
    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    @Basic
    @Column(name = "qq", nullable = true, length = 50)
    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    @Basic
    @Column(name = "home_address", nullable = true, length = 100)
    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    @Basic
    @Column(name = "work_address", nullable = true, length = 100)
    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    @Basic
    @Column(name = "remark", nullable = false, length = 100)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "adopter_state", nullable = false)
    public int getAdopterState() {
        return adopterState;
    }

    public void setAdopterState(int adopterState) {
        this.adopterState = adopterState;
    }

    @Basic
    @Column(name = "create_time", nullable = false)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "modify_time", nullable = false)
    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Basic
    @Column(name = "read_state", nullable = false)
    public int getReadState() {
        return readState;
    }

    public void setReadState(int readState) {
        this.readState = readState;
    }

    @Basic
    @Column(name = "animal_variety", nullable = true)
    public Integer getAnimalVariety() {
        return animalVariety;
    }

    public void setAnimalVariety(Integer animalVariety) {
        this.animalVariety = animalVariety;
    }

    @Basic
    @Column(name = "income", nullable = true)
    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    @Basic
    @Column(name = "live", nullable = true)
    public Integer getLive() {
        return live;
    }

    public void setLive(Integer live) {
        this.live = live;
    }

    @Basic
    @Column(name = "area_id", nullable = true)
    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    @Basic
    @Column(name = "area_name", nullable = true, length = 50)
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Basic
    @Column(name = "card_back", nullable = true, length = 100)
    public String getCardBack() {
        return cardBack;
    }

    public void setCardBack(String cardBack) {
        this.cardBack = cardBack;
    }

    @Basic
    @Column(name = "card_front", nullable = true, length = 100)
    public String getCardFront() {
        return cardFront;
    }

    public void setCardFront(String cardFront) {
        this.cardFront = cardFront;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAdoption that = (UserAdoption) o;
        return adopterState == that.adopterState &&
                readState == that.readState &&
                Objects.equals(id, that.id) &&
                Objects.equals(adoptionId, that.adoptionId) &&
                Objects.equals(openId, that.openId) &&
                Objects.equals(adopterName, that.adopterName) &&
                Objects.equals(cardNo, that.cardNo) &&
                Objects.equals(mobile, that.mobile) &&
                Objects.equals(wechat, that.wechat) &&
                Objects.equals(qq, that.qq) &&
                Objects.equals(homeAddress, that.homeAddress) &&
                Objects.equals(workAddress, that.workAddress) &&
                Objects.equals(remark, that.remark) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(modifyTime, that.modifyTime) &&
                Objects.equals(animalVariety, that.animalVariety) &&
                Objects.equals(income, that.income) &&
                Objects.equals(live, that.live) &&
                Objects.equals(areaId, that.areaId) &&
                Objects.equals(areaName, that.areaName) &&
                Objects.equals(cardBack, that.cardBack) &&
                Objects.equals(cardFront, that.cardFront);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, adoptionId, openId, adopterName, cardNo, mobile, wechat, qq, homeAddress, workAddress, remark, adopterState, createTime, modifyTime, readState, animalVariety, income, live, areaId, areaName, cardBack, cardFront);
    }
}
