package domain.bean;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "PublishTalk", schema = "dbo", catalog = "pawlove")
public class PublicTalk {
    private int id;
    private Integer publishId;
    private String publisherId;
    private int talkId;
    private String replierFrom;
    private String replierAccept;
    private String content;
    private Timestamp replyDate;
    private Integer readState;
    private Integer showState;
    private Timestamp lastModify;
    private String adoptionId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PublishID", nullable = true)
    public Integer getPublishId() {
        return publishId;
    }

    public void setPublishId(Integer publishId) {
        this.publishId = publishId;
    }

    @Basic
    @Column(name = "PublisherID", nullable = true, length = 50)
    public String getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    @Basic
    @Column(name = "TalkID", nullable = false)
    public int getTalkId() {
        return talkId;
    }

    public void setTalkId(int talkId) {
        this.talkId = talkId;
    }

    @Basic
    @Column(name = "ReplierFrom", nullable = true, length = 50)
    public String getReplierFrom() {
        return replierFrom;
    }

    public void setReplierFrom(String replierFrom) {
        this.replierFrom = replierFrom;
    }

    @Basic
    @Column(name = "ReplierAccept", nullable = true, length = 50)
    public String getReplierAccept() {
        return replierAccept;
    }

    public void setReplierAccept(String replierAccept) {
        this.replierAccept = replierAccept;
    }

    @Basic
    @Column(name = "Content", nullable = true, length = 500)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "ReplyDate", nullable = true)
    public Timestamp getReplyDate() {
        return replyDate;
    }

    public void setReplyDate(Timestamp replyDate) {
        this.replyDate = replyDate;
    }

    @Basic
    @Column(name = "ReadState", nullable = true)
    public Integer getReadState() {
        return readState;
    }

    public void setReadState(Integer readState) {
        this.readState = readState;
    }

    @Basic
    @Column(name = "ShowState", nullable = true)
    public Integer getShowState() {
        return showState;
    }

    public void setShowState(Integer showState) {
        this.showState = showState;
    }

    @Basic
    @Column(name = "LastModify", nullable = true)
    public Timestamp getLastModify() {
        return lastModify;
    }

    public void setLastModify(Timestamp lastModify) {
        this.lastModify = lastModify;
    }

    @Basic
    @Column(name = "adoption_id", nullable = true, length = 50)
    public String getAdoptionId() {
        return adoptionId;
    }

    public void setAdoptionId(String adoptionId) {
        this.adoptionId = adoptionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublicTalk that = (PublicTalk) o;
        return id == that.id &&
                talkId == that.talkId &&
                Objects.equals(publishId, that.publishId) &&
                Objects.equals(publisherId, that.publisherId) &&
                Objects.equals(replierFrom, that.replierFrom) &&
                Objects.equals(replierAccept, that.replierAccept) &&
                Objects.equals(content, that.content) &&
                Objects.equals(replyDate, that.replyDate) &&
                Objects.equals(readState, that.readState) &&
                Objects.equals(showState, that.showState) &&
                Objects.equals(lastModify, that.lastModify) &&
                Objects.equals(adoptionId, that.adoptionId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, publishId, publisherId, talkId, replierFrom, replierAccept, content, replyDate, readState, showState, lastModify, adoptionId);
    }
}
