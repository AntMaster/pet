package domain.bean;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Member {
    private int id;
    private String openId;
    private int areaId;
    private String name;
    private String nickName;
    private String headImgUrl;
    private boolean gender;
    private String mobile;
    private String email;
    private String address;
    private String remark;
    private int donationNum;
    private BigDecimal donationTotal;
    private String qrCode;
    private Timestamp modifyDate;
    private Timestamp createDate;
    private String appId;
    private int approveState;
    private int approveType;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "OpenID", nullable = false, length = 50)
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Basic
    @Column(name = "AreaID", nullable = false)
    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    @Basic
    @Column(name = "Name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "NickName", nullable = false, length = 50)
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Basic
    @Column(name = "HeadImgUrl", nullable = false, length = 500)
    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    @Basic
    @Column(name = "Gender", nullable = false)
    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "Mobile", nullable = true, length = 50)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "Email", nullable = false, length = 50)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "Address", nullable = false, length = 50)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "Remark", nullable = false, length = 500)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "DonationNum", nullable = false)
    public int getDonationNum() {
        return donationNum;
    }

    public void setDonationNum(int donationNum) {
        this.donationNum = donationNum;
    }

    @Basic
    @Column(name = "DonationTotal", nullable = false, precision = 2)
    public BigDecimal getDonationTotal() {
        return donationTotal;
    }

    public void setDonationTotal(BigDecimal donationTotal) {
        this.donationTotal = donationTotal;
    }

    @Basic
    @Column(name = "QrCode", nullable = false, length = 100)
    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    @Basic
    @Column(name = "ModifyDate", nullable = false)
    public Timestamp getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Timestamp modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Basic
    @Column(name = "CreateDate", nullable = false)
    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "AppID", nullable = false, length = 50)
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Basic
    @Column(name = "ApproveState", nullable = false)
    public int getApproveState() {
        return approveState;
    }

    public void setApproveState(int approveState) {
        this.approveState = approveState;
    }

    @Basic
    @Column(name = "ApproveType", nullable = false)
    public int getApproveType() {
        return approveType;
    }

    public void setApproveType(int approveType) {
        this.approveType = approveType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return id == member.id &&
                areaId == member.areaId &&
                gender == member.gender &&
                donationNum == member.donationNum &&
                approveState == member.approveState &&
                approveType == member.approveType &&
                Objects.equals(openId, member.openId) &&
                Objects.equals(name, member.name) &&
                Objects.equals(nickName, member.nickName) &&
                Objects.equals(headImgUrl, member.headImgUrl) &&
                Objects.equals(mobile, member.mobile) &&
                Objects.equals(email, member.email) &&
                Objects.equals(address, member.address) &&
                Objects.equals(remark, member.remark) &&
                Objects.equals(donationTotal, member.donationTotal) &&
                Objects.equals(qrCode, member.qrCode) &&
                Objects.equals(modifyDate, member.modifyDate) &&
                Objects.equals(createDate, member.createDate) &&
                Objects.equals(appId, member.appId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, openId, areaId, name, nickName, headImgUrl, gender, mobile, email, address, remark, donationNum, donationTotal, qrCode, modifyDate, createDate, appId, approveState, approveType);
    }
}
