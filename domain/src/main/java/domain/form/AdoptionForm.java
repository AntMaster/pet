package domain.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @ProjectName: pethome
 * @Package: com.shumahe.pethome.Form
 * @ClassName:
 * @Description:
 * @Author: Mr.zhangy
 * @CreateDate: 2018/4/19 18:16
 * @UpdateDate: 2018/4/19 18:16
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Data
@ApiModel(value = "送养发布表单(微信端)")
public class AdoptionForm {

    @ApiModelProperty(value = "送养ID(编辑的时候需要传送养ID)")
    private String id;

    /**
     * 编号
     */
    @ApiModelProperty(value = "宠物编号")
    private String petNo;


    /**
     * 宠物ID
     */
    @ApiModelProperty(value = "宠物ID")
    private Integer userpetId;


    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称",required =true)
    @NotBlank(message = "昵称必填")
    private String petNick;

    /**
     * 类别
     */
    @ApiModelProperty(value = "类别",required =true)
    @NotNull(message = "类别必填")
    private Integer petClassify;


    /**
     * 品种
     */
    @ApiModelProperty(value = "品种",required =true)
    @NotNull(message = "品种必填")
    private Integer petVariety;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别",required =true)
    @NotNull(message = "性别必填")
    private Integer petSex;

    /**
     * 年龄
     */
    @ApiModelProperty(value = "年龄",required =true)
    @NotNull(message = "年龄必填")
    private Integer petAge;


    /**
     * 节育状态
     */
    @ApiModelProperty(value = "绝育状态",required =true)
    @NotNull(message = "绝育状态必填")
    private Integer contraception;

    /**
     * 宠物图片
     */
    @ApiModelProperty(value = "宠物图片",required =true)
    @NotBlank(message = "宠物图片必填")
    private String petImage;


    /**
     * 描述
     */
    @ApiModelProperty(value = "图片描述")
    private String petDescription;

    /**
     * 领养要求
     */
    @ApiModelProperty(value = "领养要求")
    private String require;


    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人")
    private String contacts;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String mobile;

    /**
     * 区域ID
     */
    @ApiModelProperty(value = "区域ID")
    @NotNull(message = "区域ID必填")
    private Integer areaId;

    /**
     * 区域名称
     */
    @ApiModelProperty(value = "区域名称")
    @NotBlank(message = "区域名称必填")
    private String areaName;

    /**
     * 领养地址
     */
    @ApiModelProperty(value = "领养地址")
    private String address;

    /**
     * 是否添加宠卡
     */
    @ApiModelProperty(value = "是否添加宠卡")
    private Boolean addCard = true;


    /**
     * 是否付费领养
     */
    @ApiModelProperty(value = "是否付费领养",required = true)
    @NotNull(message = "是否付费领养必填")
    private Integer isPay;

    /**
     * 付费金额
     */
    @ApiModelProperty(value = "领养要求")
    private BigDecimal amount;

    /**
     * 疫苗状态
     */
    @ApiModelProperty(value = "疫苗状态",required = true)
    @NotNull(message = "疫苗状态必填")
    private Integer vaccineState;



}
