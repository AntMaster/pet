package domain.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * @ProjectName: pethome
 * @Package: com.shumahe.pethome.Form
 * @ClassName:
 * @Description:
 * @Author: Mr.zhangy
 * @CreateDate: 2018/4/24 12:18
 * @UpdateDate: 2018/4/24 12:18
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Data
@ApiModel(value = "用户领养申请表单")
public class UserAdoptionFormSystem {

    /**
     * 送养主键
     */
    @NotBlank(message = "送养主键必填")
    @ApiModelProperty(value = "送养主键",required = true)
    private String adoptionId;

    /**
     * openid
     */
    @NotBlank(message = "openid必填")
    @ApiModelProperty(value = "openid",required = true)
    private String openId;

    /**
     * 领养人姓名
     */
    @NotBlank(message = "联系人必填")
    @ApiModelProperty(value = "联系人")
    private String adopterName;


    /**
     * 领养人联系方式
     */
    @NotBlank(message = "联系方式必填")
    @ApiModelProperty(value = "联系方式")
    private String mobile;

    /**
     * 身份证号
     */
    @NotBlank(message = "身份证号必填")
    @ApiModelProperty(value = "身份证号")
    private String cardNo;

    /*
     * 身份证正面
     */
    //@NotBlank(message = "身份证正面必填")
    //private String cardFront;

    /*
     * 身份证背面
     */
    //@NotBlank(message = "身份证背面必填")
    //private String cardBack;

    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String wechat;


    /**
     * QQ号
     */
    @ApiModelProperty(value = "QQ号")
    private String qq;


    /**
     * 家庭住址
     */
    @NotBlank(message = "家庭住址")
    @ApiModelProperty(value = "家庭住址")
    private String homeAddress;


    /**
     * 工作地址
     */
    @NotBlank(message = "工作住址必填")
    @ApiModelProperty(value = "工作住址")
    private String workAddress;

    /**
     * 领养过宠物的种类
     */
    @ApiModelProperty(value = "领养过宠物的种类")
    private Integer animalVariety;

    /**
     * 收入
     */
    @NotNull(message = "收入必填")
    @ApiModelProperty(value = "收入",required = true)
    private Integer income;


    /**
     * 住房
     */
    @NotNull(message = "住房情况必填")
    @ApiModelProperty(value = "住房情况",required = true)
    private Integer live;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

}
