package domain.form;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
@ApiModel(value = "留言回复")
public class PublicTalkForm {

    @ApiModelProperty(value = "楼层间的回复需要talkId,另起一楼不需要传talkId")
    private Integer talkId;


    @ApiModelProperty(value = "留言发起人openId",required = true)
    @NotBlank(message = "回复人必填")
    private String replierFrom;

    @ApiModelProperty(value = "留言接收人openId",required = true)
    @NotBlank(message = "接收人必填")
    private String replierAccept;

    @ApiModelProperty(value = "留言内容",required = true)
    @NotBlank(message = "评论内容必填")
    private String content;


}
