package domain.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
@ApiModel(value = "私信回复")
public class PrivateTalkForm {

    @ApiModelProperty(value = "首次私信不用填,回复别人的私信需要填写")
    private Integer talkId;

    @ApiModelProperty(value = "私信发起人",required = true)
    @NotBlank(message = "私信发起人userIdFrom必填")
    private String userIdFrom;

    @ApiModelProperty(value = "私信接收人",required = true)
    @NotBlank(message = "私信接受人userIdAccept必填")
    private String userIdAccept;

    @ApiModelProperty(value = "私信内容",required = true)
    @NotBlank(message = "私信内容content必填")
    private String content;

}
