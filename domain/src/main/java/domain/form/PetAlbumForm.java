package domain.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("宠物相册表单")
public class PetAlbumForm {


    @NotNull(message = "宠物主键petId必填")
    @ApiModelProperty(value = "宠物主键",required = true)
    private Integer petId;


    @NotBlank(message = "相册名称name必填")
    @ApiModelProperty(value = "相册名称",required = true)
    private String name;

    @ApiModelProperty(value = "相册描述")
    private String description;

}
