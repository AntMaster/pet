package domain.form;

import domain.enums.PublishTypeEnum;
import domain.enums.pet.BaseEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("寻宠表单")
public class SearchFormPet {


    @ApiModelProperty(value = "宠物昵称", required = true)
    @NotBlank(message = "宠物昵称必填")
    private String petName;


    @ApiModelProperty(hidden = true)
    private Integer publishType = PublishTypeEnum.SEARCH_PET.getCode();

    @NotNull(message = "宠物性别必填")
    @ApiModelProperty(value = "宠物性别", required = true)
    private int petSex;


    @NotNull(message = "宠物类别必填")
    @ApiModelProperty(value = "宠物类别", required = true)
    private int classifyId;


    @NotNull(message = "宠物品种必填")
    @ApiModelProperty(value = "宠物品种", required = true)
    private int varietyId;

    @NotNull(message = "宠物颜色必填")
    @ApiModelProperty(value = "宠物颜色", required = true)
    private int petColor;


    @NotBlank(message = "丢失日期必填")
    @ApiModelProperty(value = "丢失日期", required = true)
    private String lostTime;


    @NotBlank(message = "丢失地点必填")
    @ApiModelProperty(value = "丢失地点", required = true)
    private String lostLocation;


    @NotNull(message = "地点经度必填")
    @ApiModelProperty(value = "地点经度", required = true)
    private float latitude;


    @NotNull(message = "地点纬度必填")
    @ApiModelProperty(value = "地点纬度", required = true)
    private float longitude;


    @NotBlank(message = "主人姓名必填")
    @ApiModelProperty(value = "主人姓名", required = true)
    private String ownerName;


    @NotBlank(message = "联系方式必填")
    @ApiModelProperty(value = "联系方式", required = true)
    private String ownerContact;


    @NotBlank(message = "区域ID必填")
    @ApiModelProperty(value = "区域ID", required = true)
    private Integer areaId;


    @ApiModelProperty(value = "区域名称")
    private String areaName;


    @NotBlank(message = "酬谢金额必填")
    @ApiModelProperty(value = "酬谢金额", required = true)
    private Integer amount;


    @NotEmpty(message = "图片必填")
    @ApiModelProperty(value = "图片", required = true)
    private String petImage;


    @ApiModelProperty(value = "宠物描述")
    private String petDescription;

    @NotBlank(message = "发布人主键必填")
    @ApiModelProperty(value = "发布人主键", required = true)
    private String publisherId;

}