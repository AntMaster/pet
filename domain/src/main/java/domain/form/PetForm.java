package domain.form;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("用户宠卡表单")
public class PetForm {

    @ApiModelProperty(value = "不用填,自动赋值")
    private String userId;


    @NotNull(message = "爱宠类别必填")
    @ApiModelProperty(value = "爱宠类别",required = true)
    private Integer classifyId;

    @ApiModelProperty(value = "爱宠品种")
    private Integer varietyId;


    @NotBlank(message = "爱宠头像必填")
    @ApiModelProperty(value = "爱宠头像",required = true)
    private String headImgUrl;


    @NotBlank(message = "爱宠昵称必填")
    @ApiModelProperty(value = "爱宠昵称",required = true)
    private String nickName;


    @NotNull(message = "爱宠节育状态必填")
    @ApiModelProperty(value = "爱宠节育状态",required = true)
    private Integer contraception;


    @NotNull(message = "爱宠性别必填")
    @ApiModelProperty(value = "爱宠性别",required = true)
    private Integer sex;


    @NotBlank(message = "爱宠生日必填")
    @ApiModelProperty(value = "爱宠生日",required = true)
    private String birthday;


    @NotBlank(message = "爱宠小特点必填")
    @ApiModelProperty(value = "爱宠小特点",required = true)
    private String description;

    @ApiModelProperty(value = "芯片号")
    private String chipNo;


    @NotNull(message = "疫苗状态必填")
    @ApiModelProperty(value = "疫苗状态",required = true)
    private Integer vaccineState;

}
