package domain.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "宠物照片表单")
public class PetPhotoForm {


    @NotNull(message = "相册主键albumId必填")
    @ApiModelProperty(value = "相册主键",required = true)
    private Integer albumId;


    @ApiModelProperty(value = "照片名字")
    private String name;

    @NotBlank(message = "照片路径path必填")
    @ApiModelProperty(value = "照片路径",required = true)
    private String path;

    @ApiModelProperty(value = "照片描述",required = true)
    private String description;

}
