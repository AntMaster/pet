package domain.form;

import domain.enums.data.EmptyEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel(value = "宠物搜索表单")
public class PublishSearchForm {


    /**
     * 赏金酬谢
     */
    @ApiModelProperty(value = "赏金酬谢")
    private int amount = EmptyEnum.TRUE.getCode();

    /**
     * 频道
     */
    @ApiModelProperty(value = "频道")
    private int publishType =EmptyEnum.TRUE.getCode();

    /**
     * 类别
     */
    @ApiModelProperty(value = "宠物类别")
    private int classifyId =EmptyEnum.TRUE.getCode();

    /**
     * 品种
     */
    @ApiModelProperty(value = "宠物品种")
    private int varietyId =EmptyEnum.TRUE.getCode();

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private int petSex=EmptyEnum.TRUE.getCode();

    /**
     * 颜色
     */
    @ApiModelProperty(value = "颜色")
    private int petColor=EmptyEnum.TRUE.getCode();

    /**
     * 关键字
     */
    @ApiModelProperty(value = "关键字")
    private String keyWord;

    /**
     * 层级
     */
    @ApiModelProperty(value = "层级")
    private int level=EmptyEnum.TRUE.getCode();

    /**
     * 精度
     */
    @ApiModelProperty(value = "精度")
    private Double latitude ;

    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度")
    private Double longitude;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer page = 0;

    /**
     * 条数
     */
    @ApiModelProperty(value = "条数")
    private Integer size = 10;
}
