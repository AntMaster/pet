package domain.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "企业认证表单")
public class OrganizationApproveForm {


    @NotEmpty(message = "用户主键不能为空")
    @ApiModelProperty(value = "用户主键",required = true)
    private String userId;


    @NotEmpty(message = "组织或协会名称不能为空")
    @ApiModelProperty(value = "组织或协会名称",required = true)
    private String organizationName;


    @NotEmpty(message = "图片不能为空")
    @ApiModelProperty(value = "图片",required = true)
    private String organizationImage;


    @NotEmpty(message = "负责人不能为空")
    @ApiModelProperty(value = "负责人",required = true)
    private String dutyer;


    @NotEmpty(message = "联系方式不能为空")
    @ApiModelProperty(value = "联系方式",required = true)
    private String dutyerPhone;

    @NotNull(message = "验证码必填")
    @ApiModelProperty(value = "验证码必填",required = true)
    private Integer messageCode;


}
