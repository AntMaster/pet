package domain.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "寻主表单")
public class SearchFormMaster {


    @NotBlank(message = "宠物图片必填")
    @ApiModelProperty(value = "宠物图片", required = true)
    private String petImage;


    @NotNull(message = "宠物类别必填")
    @ApiModelProperty(value = "宠物类别", required = true)
    private int classifyId;

    @ApiModelProperty(value = "宠物品种")
    private int varietyId;

    @NotBlank(message = "发现时间必填")
    @ApiModelProperty(value = "发现时间", required = true)
    private String lostTime;


    @NotBlank(message = "发现地点必填")
    @ApiModelProperty(value = "发现地点", required = true)
    private String lostLocation;


    @ApiModelProperty(value = "地点经度")
    private float latitude;


    @ApiModelProperty(value = "地点纬度")
    private float longitude;


    @NotBlank(message = "发现人必填")
    @ApiModelProperty(value = "发现人", required = true)
    private String ownerName;


    @NotBlank(message = "联系方式必填")
    @ApiModelProperty(value = "联系方式", required = true)
    private String ownerContact;


    @NotBlank(message = "发布人主键必填")
    @ApiModelProperty(value = "发布人主键", required = true)
    private String publisherId;

    @ApiModelProperty(value = "宠物描述")
    private String petDescription;

}
