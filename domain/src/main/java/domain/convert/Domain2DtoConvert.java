package domain.convert;


import domain.bean.Member;
import domain.bean.PetSearch;
import domain.dto.MemberDto;
import domain.dto.PetSearchDto;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static util.BeanUtils.copyProperties;

public class Domain2DtoConvert {

    public static PetSearchDto convertSearch(PetSearch search) {
        PetSearchDto dto = new PetSearchDto();
        copyProperties(search, dto);
        return dto;
    }

    public static MemberDto convertMember(Member member) {
        MemberDto dto = new MemberDto();
        copyProperties(member, dto);
        return dto;
    }

    public static List<PetSearchDto> convertSearches(List<PetSearch> searches) {
        if (searches.isEmpty())
            return new ArrayList<>();
        return searches.stream().map(Domain2DtoConvert::convertSearch).collect(toList());
    }


    public static List<MemberDto> convertMembers(List<Member> members) {
        if (members.isEmpty())
            return new ArrayList<>();
        return members.stream().map(Domain2DtoConvert::convertMember).collect(toList());
    }

}
