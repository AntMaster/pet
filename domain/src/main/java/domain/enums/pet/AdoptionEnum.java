package domain.enums.pet;

import lombok.Getter;

/**
 * @ProjectName: pethome
 * @Package: com.shumahe.pethome.Enums
 * @ClassName:
 * @Description:
 * @Author: Mr.zhangy
 * @CreateDate: 2018/6/26 14:45
 * @UpdateDate: 2018/6/26 14:45
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public enum AdoptionEnum {

    ;

    /**
     * 领养来源
     */
    @Getter
    public enum AdoptFromEnum {

        UNKNOWN(0, "未知"),
        ADMIN(1, "后台人员"),
        COMMON(2, "普通用户");

        private int code;
        private String message;

        AdoptFromEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }


    /**
     * 宠物领养审批状态
     */
    @Getter
    public enum AdoptionStateEnum {

        ALL(-1, "全部"),
        NOT_ADOPT(0, "待领养"),
        ADOPTING(1, "领养中"),
        ADOPTED(2, "已领养");


        private Integer code;
        private String message;

        AdoptionStateEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    /**
     * 收入情况
     */
    @Getter
    public enum IncomeEnum {

        UNDER_FIVE(1, "五千及以下"),
        FIVE_NIGH(2, "5000至8000"),
        UP_NIGH(3, "8000及以上");

        private int code;
        private String message;

        IncomeEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    /**
     * 住房情况
     */
    @Getter
    public enum LiveEnum {

        HOUSE(1, "自有住房"),
        RENT_WITH_FRIEND(2, "合租"),
        RENT_ALONE(3, "独租");


        private int code;
        private String message;

        LiveEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }

    }


    /**
     * 育苗接种情况
     */
    @Getter
    public enum VaccineEnum {

        UNKNOWN(0, "未知"),
        NO(1, "未接种"),
        DOING(2, "进行中"),
        DONE(3, "已完成");

        private int code;
        private String message;

        VaccineEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

}
