package domain.enums.pet;

import lombok.Getter;

public enum BaseEnum {

    ;

    @Getter
    public enum PetAgeEnum {

        UNKNOWN(-1, "未知"),
        ZERO(0, "不到一岁"),
        ONE(1, "一岁"),
        TWO(2, "两岁"),
        THREE(3, "三岁"),
        FOUR(4, "四岁"),
        FIVE(5, "五岁"),
        SIX(6, "六岁"),
        SEVEN(7, "七岁及以上");

        private int code;

        private String message;

        PetAgeEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }


    @Getter
    public enum PetClassifyEnum {

        CAT(2, "猫"),
        DOG(3, "狗"),
        RABBIT(4, "兔子"),
        MOUSE(6, "老鼠");
        private int code;
        private String message;

        PetClassifyEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    @Getter
    public enum PetSexEnum {

        FEMALE(0, "母"),
        MALE(1, "公"),
        UNKNOWN(2, "未知");

        private int code;
        private String message;

        PetSexEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }


    @Getter
    public enum ContraceptionStateEnum {

        FALSE(0, "未节育"),
        TRUE(1, "已节育"),
        UNKNOWN(2, "未知");

        private int code;
        private String message;

        ContraceptionStateEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }


    }


}
