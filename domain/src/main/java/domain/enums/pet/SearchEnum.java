package domain.enums.pet;

import lombok.Getter;

public enum SearchEnum {

    ;

    @Getter
    public enum FindStateEnum {

        FOUND(1, "已找到"),
        NOT_FOUND(0, "未找到");

        private int code;

        private String message;

        FindStateEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }
}
