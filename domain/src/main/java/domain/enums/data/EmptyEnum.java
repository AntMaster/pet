package domain.enums.data;

import lombok.Getter;

@Getter
public enum EmptyEnum {

    TRUE(-1, "是"),
    FALSE(0, "否");

    private int code;

    private String message;

    EmptyEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
