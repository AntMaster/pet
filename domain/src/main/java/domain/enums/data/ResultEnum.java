package domain.enums.data;

import lombok.Getter;

/**
 * Created by zhangyu
 * 2018-02-23 21:50
 */
@Getter
public enum ResultEnum {

    FAILURE(0, "失败"),

    SUCCESS(1, "成功"),

    /**
     * 2xxx power error description
     */
    PASSWORD_EMPTY(25, "密码为空"),

    LOGOUT_ERROR(26, "密码错误"),

    /**
     * 3xxx data error description
     */
    PARAM_EMPTY(31, "参数为空"),

    PARAM_ERROR(32, "参数不正确"),

    RESULT_EMPTY(33, "结果为空"),

    RESULT_EXISTS(34, "结果已存在"),

    /**
     * 4xxx wechat error description
     */
    OPENID_EMPTY(61, "openId为空"),

    OPENID_ERROR(62, "openId不正确"),

    WECHAT_MP_ERROR(63, "微信公众账号配置错误");

   /* WECHAT_MP_ERROR(63, "微信公众账号方面错误"),

    WXPAY_NOTIFY_MONEY_VERIFY_ERROR(61, "微信支付异步通知金额校验不通过");*/


    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
