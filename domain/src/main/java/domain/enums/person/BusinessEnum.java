package domain.enums.person;

import lombok.Getter;

public enum BusinessEnum {
    ;
    @Getter
    public enum ApproveStateEnum {

        FAILURE(0, "认证失败"),
        SUCCESS(1, "认证成功"),
        WAITING(2, "待认证");

        private int code;

        private String message;

        ApproveStateEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    @Getter
    public enum DynamicTypeEnum {

        LIKE(1, "关注"),
        SHARE(2, "转发"),
        CANCEL(3, "取关");

        private int code;

        private String message;

        DynamicTypeEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }



}
