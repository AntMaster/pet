package domain.enums.person;

import lombok.Getter;

public enum ApproveEnum {

    ;

    /**
     * 领养审批状态
     */
    @Getter
    public enum AdoptionStateEnum {

        WAITING(1, "待审核"),
        ADOPTING(2, "领养中"),//通过操作
        ADOPTED(3, "已领养"),//确认操作
        FAILURE_REJECT(4, "资料未通过"),//驳回操作
        FAILURE_CREDIT(5, "失信原因"),//撤销
        FAILURE_OTHER(6, "其他原因"),//撤销
        FAILURE_ADOPTED(7, "已被领养"),//领养成功后自动更新
        FAILURE_CANCEL(8, "取消领养");//取消领养

        private int code;
        private String message;

        AdoptionStateEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }
}
