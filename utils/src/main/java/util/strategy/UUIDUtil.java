package util.strategy;

import java.util.UUID;

public class UUIDUtil {



    public static String getUUID() {
        return UUID.randomUUID().toString().toUpperCase();
    }

    //得到指定数量的UUID，以数组的形式返回
    public static String[] getUUIDs(int num) {

        if (num <= 0)
            return null;

        String[] uuidArr = new String[num];

        for (int i = 0; i < uuidArr.length; i++) {
            uuidArr[i] = getUUID();
        }
        return uuidArr;
    }

}
