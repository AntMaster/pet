package util;

import jdk.nashorn.internal.ir.Node;

import java.util.LinkedList;

public class Josephus {

    public static void simulate(int n, int m) {
        int answer = 0;
        for (int i = 1; i <= n; i++) {
            answer = (answer + m) % i;
            System.out.println("Survival: " + answer);
        }
    }

    public static void main(String[] args) {
        //simulate(7, 3);
        System.out.println(fibonacci(5));
    }


    private static int fibonacci(int day) {
        if(day==0){ //F(0)=0
            return 0;
        }else if (day==1||day==2){//F(1)=1
            return 1;
        }else {
            return fibonacci(day-1)+fibonacci(day-2); //F(n)=F(n-1)+F(n-2)
        }
    }
}
