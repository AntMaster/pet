package util;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

@Slf4j
public class BeanMapConvert {

    /**
     * 将对象装换为map
     * @param bean
     * @return
     */
   /* public static <T> Map<String, Object> beanToMap(T bean) {
    }
*/

    /**
     * 将map装换为javabean对象
     *
     * @param map
     * @param bean
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, T bean) {
        try {
            BeanUtils.populate(bean, map);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            log.error("mapToBean类型转换错误,{},{}", map, bean);
        }
        return bean;
    }

    /**
     *
     */
    public static <T> List<Map<String, Object>> objectsToMaps(List<T> objList) {
        return null;
    }

    /**
     * 将List<Map<String,Object>>转换为List<T>
     *
     * @param maps
     * @param clazz
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static <T> List<T> mapsToObjects(List<Map<String, Object>> maps, Class<T> clazz) throws InstantiationException, IllegalAccessException {
        return null;
    }

}
