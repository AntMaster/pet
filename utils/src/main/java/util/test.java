package util;

import java.util.ArrayList;
import java.util.List;

public class test {

    public static void main(String[] args) {
        getLuckyNum(8, 3);
    }

    private static void getLuckyNum(int num, int n) {
        // 创建集合用于存储1-num的对象
        List<Integer> list = new ArrayList<>();
        // 将1-num的数存到集合中
        for (int i = 1; i <= num; i++) {
            list.add(i);
        }
        // 定义一个计数器,用来计数,只要是能被n整除的数就移除
        int count = 1;
        // 只要集合中的对象数超过1就要不断的循环计数移除,直到剩下最后一个
        for (int i = 0; list.size() != 1; i++) {
            // 如果i增长到集合最大索引+1时就重新归零
            if (i == list.size()) {
                i = 0;
            }
            if (count % n == 0) {
                list.remove(i--);
            }
            count++;
        }
        System.out.println(list.get(0));
    }
}
