package util.annotation;


import java.lang.annotation.*;

/**
 * @ProjectName: pethome
 * @Package: com.shumahe.pethome.Annotation
 * @ClassName:
 * @Description:
 * @Author: Mr.zhangy
 * @CreateDate: 2018/4/27 15:55
 * @UpdateDate: 2018/4/27 15:55
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ClearRedis {

    String[] keyKeywords() default "";
}


