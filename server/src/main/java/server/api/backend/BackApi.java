package server.api.backend;

import dao.PetAdoptionJpa;
import domain.bean.PetAdoption;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/back")
@Api(value = "领养value",tags = {"领养"},produces = "领养produces",consumes = "领养consumes",protocols = "领养protocols",hidden = false)
public class BackApi {

    private final PetAdoptionJpa petAdoptionJpa;

    @Autowired
    public BackApi(PetAdoptionJpa petAdoptionJpa) {
        this.petAdoptionJpa = petAdoptionJpa;
    }


    @GetMapping("/pet/{id}")
    @ApiOperation(value = "新增领养")
    public PetAdoption selectOnePet(@PathVariable String id) {

        PetAdoption one = petAdoptionJpa.getOne("123");
        return petAdoptionJpa.getOne("123");
        //return petAdoptionJpa.getOne(id);
    }


    @PostMapping("/pet")
    public PetAdoption insertOnePet() {
        PetAdoption pet = new PetAdoption();
        pet.setPetNick("张三");
        return petAdoptionJpa.save(pet);
    }

    @PutMapping("/pet/{id}")
    public PetAdoption updateOnePet(@PathVariable String id) {
        PetAdoption one = petAdoptionJpa.getOne(id);
        one.setPetNick("修改的名字");
        return petAdoptionJpa.save(one);

    }

    @PatchMapping("/pet/{id}")
    public PetAdoption updateOnePetPart(@PathVariable String id) {
        PetAdoption one = petAdoptionJpa.getOne(id);
        one.setPetNick("修改的名字patch");
        return petAdoptionJpa.save(one);
    }


}
