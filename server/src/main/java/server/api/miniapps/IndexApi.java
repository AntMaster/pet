package server.api.miniapps;

import domain.bean.R;
import domain.form.PublishSearchForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import server.service.impl.IndexServiceImpl;

import java.util.Map;

@RestController
@RequestMapping("/app/index")
@Api(tags = {"首页"})
public class IndexApi {


    private final IndexServiceImpl indexService;

    @Autowired
    public IndexApi(IndexServiceImpl indexService) {
        this.indexService = indexService;
    }

    @GetMapping("/search/keyword")
    @ApiOperation(value = "按关键字搜索",notes = "selectPublishByKeyword")
    private R selectPublishByKeyword(@ApiParam PublishSearchForm form) {

        Map<String, Object> res = indexService.selectPublishByKeyword(form);
        return R.isOk();
    }


    @GetMapping("/search/location")
    @ApiOperation(value = "按地理位置搜索",notes = "selectPublishByLocation")
    private R selectPublishByLocation(@ApiParam PublishSearchForm form) {

        Map<String, Object> res = indexService.selectPublishByLocation(form);
        return R.isOk();
    }

}
