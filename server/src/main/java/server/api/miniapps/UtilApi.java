package server.api.miniapps;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import server.service.impl.UtilServiceImpl;

@RestController
@RequestMapping("/api/util")
@Slf4j
public class UtilApi {

    private final UtilServiceImpl utilService;

    @Autowired
    public UtilApi(UtilServiceImpl utilService) {
        this.utilService = utilService;
    }


    @ApiOperation(value = "通用文件上传", notes = "文件夹名称", tags = "工具类")
    @PostMapping("/fileUpload")
    public void commonFileUpload(@ApiParam @RequestParam(value = "file") MultipartFile file,
                                 @ApiParam @RequestParam(value = "uploadType") String fileType,
                                 @ApiParam @RequestParam(value = "folder") String folder) {
        utilService.commonFileUpload(file,fileType,folder);
    }


}
