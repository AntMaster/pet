package server.api.miniapps;

import domain.bean.R;
import domain.bean.PetSearch;
import domain.enums.data.ResultEnum;
import domain.form.SearchFormPet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import server.config.handler.PetException;
import server.service.impl.SearchServiceImpl;
import util.BeanMapConvert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/app/search")
@Api(tags = {"寻主寻宠"})
public class SearchApi {

    private final SearchServiceImpl searchService;

    @Autowired
    public SearchApi(SearchServiceImpl searchService) {
        this.searchService = searchService;
    }

    @PostMapping("/search")
    @ApiOperation(value = "创建寻主寻宠", notes = "insertOneSearch")
    public R<Object> insertOneSearch(@ApiParam @RequestBody SearchFormPet searchFormPet, BindingResult bindingResult) {

        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        if (!fieldErrors.isEmpty()) {
            List<String> collect = fieldErrors.stream().map(FieldError::getDefaultMessage).collect(toList());
            throw new PetException(ResultEnum.PARAM_EMPTY.getCode(), Arrays.toString(collect.toArray()));
        }

        return R.isOk(searchService.insertOneSearch(searchFormPet));
    }

    @GetMapping("/search/{searchId}")
    @ApiOperation(value = "查询寻主寻宠", notes = "selectOneSearch")
    public Optional<PetSearch> selectOneSearch(@PathVariable @ApiParam Integer searchId) {

        return searchService.selectOneSearch(searchId);

    }

    @PutMapping("/search/")
    @ApiOperation(value = "修改寻主寻宠", notes = "updateOneSearch")
    public PetSearch updateOneSearch(@RequestBody @ApiParam Map<String, Object> seachMap)  {

        PetSearch petSearch = BeanMapConvert.mapToBean(seachMap, new PetSearch());
        return searchService.updateOneSearch(petSearch);

    }

    @PatchMapping("/search/")
    @ApiOperation(value = "修改寻主寻宠", notes = "updateOneSearch")
    public PetSearch updateOneSearchPart(@RequestBody @ApiParam PetSearch petSearch) {
        return searchService.updateOneSearch(petSearch);
    }

}
