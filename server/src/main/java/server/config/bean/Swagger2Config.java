package server.config.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class Swagger2Config {

    private static final String SWAGGER2_API_BASEPACKAGE = "server";
    private static final String SWAGGER2_API_TITLE = "Spring Boot中使用Swagger2构建RESTful APIs";
    private static final String SWAGGER2_API_DESCRIPTION = "宠爱有家小程序API";
    private static final String SWAGGER2_API_VERSION = "0.0.1SNAPSHOT";
    /**
     * createRestApi
     *
     * @return
     */
    @Bean
    public Docket createRestApi() {
        /*
         * UI页面显示信息
         */
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(SWAGGER2_API_BASEPACKAGE))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * apiInfo
     *
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(SWAGGER2_API_TITLE)
                .description(SWAGGER2_API_DESCRIPTION)
                .contact(contact())
                .version(SWAGGER2_API_VERSION)
                .build();
    }

    private Contact contact() {
        String name = "AntMaster";
        String url = "http://www.antmaster.cn";
        String email = "rainpluszhang@foxmain.com";
        return new Contact(name, url, email);
    }
}


