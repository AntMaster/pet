package server.config.handler;

import domain.enums.data.ResultEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
public class PetException extends RuntimeException {

    private static final long serialVersionUID = -1918619858382172175L;
    private int code;

    public PetException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public PetException(int code, String message) {
        super(message);
        this.code = code;
    }
}
