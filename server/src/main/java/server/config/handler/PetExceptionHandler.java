package server.config.handler;

import domain.bean.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class PetExceptionHandler extends RuntimeException {

    private static final long serialVersionUID = 8670173506637267705L;

    @ExceptionHandler(value = RuntimeException.class)
    @ResponseBody
    public R petHomeException(Exception e) {
        if (e instanceof PetException) {

            log.error("【业务异常】{}", e.getMessage());
            return R.isFail(e);
            //return R.error(petHomeException.getCode(), petHomeException.getMessage());
        } else {
            log.error("【非业务错误】{}", e);
            //return ResultVOUtil.error(999, "服务器已经飞行到外太空,请稍后再试!");
            return R.isFail(e);
        }
    }


}
