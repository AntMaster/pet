package server.service;


import domain.bean.PetSearch;
import domain.bean.PrivateTalk;
import domain.bean.PublicTalk;
import domain.form.PrivateTalkForm;
import domain.form.PublicTalkForm;
import domain.form.SearchFormPet;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface SearchService {

    //----------------------------------base CRUD-----------------------------------------------------
    PetSearch insertOneSearch(SearchFormPet form);

    PetSearch updateOneSearch(PetSearch petSearch);

    PetSearch selectOneSearch(Integer searchId);

    List<PetSearch> selectSearchListByIds(List<Integer> searchIds);

    Map<Integer, PetSearch> selectSearchMapByIds(List<Integer> searchIds);

    Map<Integer, PetSearch> selectSearchMap(List<PetSearch> searches);

    //----------------留言
    PublicTalk insertOnePublicTalk(PublicTalkForm form);

    PublicTalk updateOnePublicTalk(PublicTalk publicTalk);

    PublicTalk selectOnePublicTalk(Integer talkId);

    //----------------私信
    PrivateTalk insertOnePrivateTalk(PrivateTalkForm form);

    PrivateTalk updateOnePrivateTalk(PrivateTalk privateTalk);

    PrivateTalk selectOnePrivateTalk(Integer talkId);





}
