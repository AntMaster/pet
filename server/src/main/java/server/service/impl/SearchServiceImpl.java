package server.service.impl;


import dao.PetSearchJpa;
import dao.PrivateTalkJpa;
import dao.PublicTalkJpa;
import domain.bean.PetSearch;
import domain.bean.PrivateTalk;
import domain.bean.PublicTalk;
import domain.form.PrivateTalkForm;
import domain.form.PublicTalkForm;
import domain.form.SearchFormPet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.service.SearchService;
import util.BeanUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class SearchServiceImpl implements SearchService {

    private final PetSearchJpa petSearchJpa;

    private final PublicTalkJpa publicTalkJpa;

    private final PrivateTalkJpa privateTalkJpa;

    @Autowired
    public SearchServiceImpl(PetSearchJpa petSearchJpa, PublicTalkJpa publicTalkJpa, PrivateTalkJpa privateTalkJpa) {
        this.petSearchJpa = petSearchJpa;
        this.publicTalkJpa = publicTalkJpa;
        this.privateTalkJpa = privateTalkJpa;
    }


    /**
     * insert 寻主寻宠
     *
     * @param form
     * @return
     */
    @Override
    public PetSearch insertOneSearch(SearchFormPet form) {
        PetSearch petSearch = new PetSearch();
        BeanUtils.copyProperties(form, petSearch);
        return petSearchJpa.save(petSearch);
    }

    /**
     * update 寻主寻宠
     *
     * @param petSearch
     * @return
     */
    @Override
    public PetSearch updateOneSearch(PetSearch petSearch) {
        Optional<PetSearch> pet = petSearchJpa.findById(petSearch.getId());
        if (!pet.isPresent())
            return petSearch;

        BeanUtils.copyProperties(petSearch, pet.get());
        return petSearchJpa.save(pet.get());
    }

    /**
     * select 寻主寻宠
     *
     * @param searchId
     * @return
     */
    @Override
    public PetSearch selectOneSearch(Integer searchId) {
        return petSearchJpa.findById(searchId).orElse(null);
    }

    /**
     * select  寻主寻宠 list
     *
     * @param searchIds
     * @return
     */
    @Override
    public List<PetSearch> selectSearchListByIds(List<Integer> searchIds) {
        List<PetSearch> allById = petSearchJpa.findAllById(searchIds);
        if (allById.isEmpty())
            return new ArrayList<>();

        return allById;
    }

    /**
     * select  寻主寻宠 map
     *
     * @param searchIds
     * @return
     */
    @Override
    public Map<Integer, PetSearch> selectSearchMapByIds(List<Integer> searchIds) {

        List<PetSearch> allById = petSearchJpa.findAllById(searchIds);
        if (allById.isEmpty())
            return new HashMap<>();

        return allById.stream().collect(Collectors.toMap(PetSearch::getId, Function.identity()));
    }

    /**
     * select  寻主寻宠 list
     *
     * @param searches
     * @return
     */
    @Override
    public Map<Integer, PetSearch> selectSearchMap(List<PetSearch> searches) {
        return searches.stream().collect(Collectors.toMap(PetSearch::getId, Function.identity()));
    }

    @Override
    public PublicTalk insertOnePublicTalk(PublicTalkForm form) {
        PublicTalk talk = new PublicTalk();
        BeanUtils.copyProperties(form, talk);
        return null;
    }

    @Override
    public PublicTalk updateOnePublicTalk(PublicTalk publicTalk) {
        return null;
    }

    @Override
    public PublicTalk selectOnePublicTalk(Integer talkId) {
        return null;
    }

    @Override
    public PrivateTalk insertOnePrivateTalk(PrivateTalkForm form) {
        return null;
    }

    @Override
    public PrivateTalk updateOnePrivateTalk(PrivateTalk privateTalk) {
        return null;
    }

    @Override
    public PrivateTalk selectOnePrivateTalk(Integer talkId) {
        return null;
    }

    public static void main(String[] args) {

        PetSearch petSearch1 = new PetSearch();
        petSearch1.setPetName("1");

        PetSearch petSearch2 = new PetSearch();
        petSearch2.setPetName("2");
        petSearch2.setAmount(123);

        PetSearch petSearch3 = new PetSearch();
        petSearch3.setPetName("3");
    }
}


