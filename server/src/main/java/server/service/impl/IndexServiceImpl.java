package server.service.impl;

import dao.PetSearchJpa;
import domain.bean.PetSearch;
import domain.enums.data.EmptyEnum;
import domain.enums.data.ResultEnum;
import domain.enums.data.ShowStateEnum;
import domain.enums.pet.SearchEnum;
import domain.form.PublishSearchForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import server.config.handler.PetException;
import server.service.IndexService;

import javax.persistence.criteria.Predicate;
import java.util.Map;

@Service
public class IndexServiceImpl implements IndexService {

    private final PetSearchJpa petSearchJpa;

    @Autowired
    public IndexServiceImpl(PetSearchJpa petSearchJpa) {
        this.petSearchJpa = petSearchJpa;
    }


    @Override
    public Map<String, Object> selectPublishByKeyword(PublishSearchForm form) {
        common(form, false);
        Page<PetSearch> all = petSearchJpa.findAll(PageRequest.of(form.getPage(), form.getSize()));
        return null;
    }

    @Override
    public Map<String, Object> selectPublishByLocation(PublishSearchForm form) {
        common(form, true);
        return null;
    }

    private void common(PublishSearchForm form, boolean isLocationSearch) {

        String keyWord = form.getKeyWord();
        int publishType = form.getPublishType();
        int classifyId = form.getClassifyId();
        int varietyId = form.getVarietyId();
        int petSex = form.getPetSex();

        Specification<PetSearch> specification = (root, query, cb) -> {


            Predicate basePredicate = cb.equal(root.get("publishState"), ShowStateEnum.SHOW.getCode());

            if (isLocationSearch) {
                basePredicate = cb.and(basePredicate, cb.isTrue(root.get("publishType").isNotNull()));//坐标不为空
                basePredicate = cb.and(basePredicate, cb.equal(root.get("showState"),SearchEnum.FindStateEnum.NOT_FOUND.getCode()));//已找到的不显示
            }


            if (publishType != EmptyEnum.TRUE.getCode())
                basePredicate = cb.and(basePredicate, cb.equal(root.get("publishType"), publishType));

            if (classifyId != EmptyEnum.TRUE.getCode())
                basePredicate = cb.and(basePredicate, cb.equal(root.get("classifyId"), classifyId));

            if (varietyId != EmptyEnum.TRUE.getCode())
                basePredicate = cb.and(basePredicate, cb.equal(root.get("varietyId"), varietyId));

            if (petSex != EmptyEnum.TRUE.getCode())
                basePredicate = cb.and(basePredicate, cb.equal(root.get("petSex"), petSex));

            if (!(keyWord == null || keyWord.isEmpty())) {
                Predicate petName = cb.like(root.get("petName"), "%" + keyWord + "%");
                Predicate lostLocation = cb.like(root.get("lostLocation"), "%" + keyWord + "%");
                Predicate ownerName = cb.like(root.get("ownerName"), "%" + keyWord + "%");
                basePredicate = cb.and(basePredicate, cb.or(cb.or(petName, lostLocation), ownerName));
            }

            query.orderBy(cb.desc(root.get("createTime")));
            return basePredicate;
        };

        Page<PetSearch> all = petSearchJpa.findAll(specification, PageRequest.of(form.getPage(), form.getSize()));
        if (all.getContent().isEmpty()) {
            throw new PetException(ResultEnum.RESULT_EMPTY);
        }

        System.out.println(all);
    }


    void tst() {


    }
}
