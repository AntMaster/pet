package server.service.impl;

import com.google.common.collect.ImmutableList;
import domain.enums.data.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import server.config.handler.PetException;
import server.service.UtilService;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

import static java.lang.System.currentTimeMillis;
import static java.time.format.DateTimeFormatter.ofPattern;
import static util.FileUtil.uploadFile;

@Service
@Slf4j
public class UtilServiceImpl implements UtilService {



    @Value("${web.upload-path}")
    private String picturePath;

    private final static List<String> fileTypes = ImmutableList.of("picture", "doc", "xml");
    private final static List<String> folders = ImmutableList.of("authentication", "adoption", "search", "pet", "petAlbum", "petPhoto");


    /*
     * @Description: 通用上传方法
     * @Author: zhangy
     * @CreateDate: 2018/4/25 18:11
     * @UpdateUser:
     * @UpdateDate: 2018/4/25 18:11
     * @UpdateRemark: The modified content
     */
    @Override
    public void commonFileUpload(MultipartFile file, String fileType, String folder) {


        if (!fileTypes.contains(fileType))
            throw new PetException(ResultEnum.FAILURE);


        if (!folders.contains(fileType))
            throw new PetException(ResultEnum.FAILURE);

        String[] split = "\\.".split(file.getOriginalFilename());
        String fileName = LocalDate.now().format(ofPattern("yyyyMMdd")) + currentTimeMillis() + "." + split[split.length - 1];
        String filePath = picturePath + File.separator + fileType + File.separator + folder + File.separator;

        try {
            uploadFile(file.getBytes(), filePath, fileName);
        } catch (Exception e) {
            log.error("文件上传失败");
            throw new PetException(ResultEnum.FAILURE.getCode(), "文件上传失败");
        }

        String path = "upload/" + fileType + "/" + folder + "/" + fileName;
    }
}
