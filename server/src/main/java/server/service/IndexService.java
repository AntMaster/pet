package server.service;

import domain.form.PublishSearchForm;

import java.util.Map;

public interface IndexService {

    Map<String,Object> selectPublishByKeyword(PublishSearchForm form);

    Map<String,Object> selectPublishByLocation(PublishSearchForm form);
}
